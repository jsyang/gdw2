function AIState(params){
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }
}

module.exports = AIState;