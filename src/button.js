var atom = require('atom');

function Button(params) {
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }
}

Button.prototype = {
    x : 0,
    y : 0,
    w : 0,
    h : 0
};

Button.prototype.containsPoint = function (x, y) {
    return this.x <= x && x <= this.x + this.w &&
            this.y <= y && y <= this.y + this.h;
};

Button.prototype.draw = function(){
    atom.context.drawImage(atom.GFX[this.image], this.x, this.y);
};

module.exports = Button;