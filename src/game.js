var random = require('./random');
var atom = require('atom');

var GameActor = require('./game_actor');
var GameUI = require('./game_ui');
var GameLayout = require('./game_layout');

var Button = require('./button');
var Instructions = require('./instructions');


function Game() {
    atom.input.bind(atom.button.LEFT, 'mouseleft');
    //atom.input.bind(atom.button.TOUCHING, 'touchfinger');

    window.onblur = this.stop.bind(this);
    window.onfocus = this.run.bind(this);

    // Can't play yet since the tiles are jumbled originally
    this.disableStartButton();

    this.instructions = new Instructions({
        game : this,
        verticaloffset: 0
    });

    this.actor = GameActor(this);
    this.ui = GameUI(this);
    this.layout = GameLayout(this);
}

Game.prototype = Object.create(atom.Game.prototype);

Game.prototype.update = function (dt) {
    this.mode[this.mode.current].call(this, dt);
};

Game.prototype.draw = function() {
    atom.context.clear();
    this.layout.tiles.forEach(function (tile) {
        tile.draw();
    });

    var key;
    for(key in this.buttons) {
        this.buttons[key].draw();
    }
    this.layout.finalTiles.forEach(function (tile) {
        tile.draw();
    });

    this.instructions.draw();
};

Game.prototype.buttons = {
    randomLayout : new Button({
        x       : window.innerWidth - 248,
        y       : window.innerHeight - 60,
        hOffset : 248,
        w       : 138,
        h       : 50,
        image   : 'button_random',
        clicked : 'generateRandomLayout'
    }),

    start : new Button({
        x       : window.innerWidth - 347,
        y       : window.innerHeight - 60,
        hOffset : 347,
        w       : 90,
        h       : 50,
        image   : 'button_play'
    }),

    help : new Button({
        x       : window.innerWidth - 100,
        y       : window.innerHeight - 60,
        hOffset : 100,
        w       : 89,
        h       : 50,
        image   : 'button_help',
        clicked : 'showTilePlacementHelp'
    })
};

Game.prototype.hasMovesLeft = function () {

};

var funcName;
var UILogic = require('./uilogic');
for(funcName in UILogic) {
    Game.prototype[funcName] = UILogic[funcName];
}

Game.prototype.mode = require('./game_mode');
Game.prototype.user = require('./game_user');
Game.prototype.layout = require('./game_layout');

module.exports = Game;