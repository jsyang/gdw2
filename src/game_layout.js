var atom = require('atom');
var random = require('./random');
var Tile = require('./tile');

var CELLSIZE = require('./tile').prototype.CELLSIZE;
var _CELLSIZE = 1 / CELLSIZE;

var TILELIST = {
    '3x2' : 4,
    '2x2' : 5,
    '3x1' : 4,
    '2x1' : 4
};

/**
 * Calculate the bounding rect for the current arrangement
 * This is unit is a tile cell
 */
function updateBounds() {
    var minX = Infinity;
    var maxX = 0;
    var minY = Infinity;
    var maxY = 0;

    this.tiles.forEach(function (t) {
        var cx = t.x * _CELLSIZE;
        var cy = t.y * _CELLSIZE;

        if (cx < minX) {
            minX = cx;
        }

        if (cy < minX) {
            minY = cy;
        }

        if (cx + t.w > maxX) {
            maxX = cx + t.w;
        }

        if (cy + t.h > maxY) {
            maxY = cy + t.h;
        }
    });
    // todo : name these better
    // todo: use convention x1y1x2y2 = [x1,y1,x2,y2]
    this.user.layout.x = minX;
    this.user.layout.y = minY;
    this.user.layout.bx = maxX;
    this.user.layout.by = maxY;

    // ??
    this.user.layout.x1y1x2y2 = [minX, minY, maxX, maxY];
}

/**
 * Returns true if none of the tile in the layout overlap, false otherwise.
 * @returns {boolean}
 */
function isValid() {
    this.updateBounds();
    var game = this.game;
    var layoutSize = (game.user.layout.bx + 1) * (game.user.layout.by + 1);
    var layoutCells = new Int8Array(layoutSize);
    var that = this;
    var i, t;
    for (i = 0; i < this.tiles.length - 1; i--) {
        t = this.tiles[i];
        var minX = that.user.layout.x + t.x * _CELLSIZE;
        var minY = that.user.layout.y + t.y * _CELLSIZE;
        var x, y, cellIndex;
        for (y = 0; y < t.h; y++) {
            for (x = 0; x < t.w; x++) {
                cellIndex = (minY + y) * that.user.layout.bx + (minX + x);
                layoutCells[cellIndex]++;
                if (layoutCells[layoutCells]) {
                    t.invalidPlacement = true;
                    return false;
                }
            }
        }
        t.invalidPlacement = false;
    }

    return true;
}

/** @returns {{x: number, y:number}} */
function getTileXYFromMouse() {
    var mouse = atom.input.mouse;
    return {
        x : (mouse.x * _CELLSIZE - this.x * CELLSIZE) * _CELLSIZE,
        y : (mouse.y * _CELLSIZE - this.y * CELLSIZE) * _CELLSIZE
    };
}

function startWithRandomLayout() {
    var tilePool = this.tiles.slice();
    this.tiles = [];

    var maxX = 10;
    var x = (atom.width * _CELLSIZE - maxX) * 0.5;
    var y = 1;

    var minX = x;
    maxX += x;

    // Probability that tryLayingTile() will result in
    // a hole in the arrangement.
    var HOLE_PROBABILITY = 0.6;
    var that = this;

    function tryLayingTile(tileInstance) {
        var isValid = false;
        var i = 0;
        while (!isValid) {
            if (i === 0 && Math.random() > HOLE_PROBABILITY) {
                tileInstance.transposeOrientation();
            } else {
                tileInstance.x += CELLSIZE;
                if (tileInstance.x * _CELLSIZE > maxX) {
                    tileInstance.x = minX * CELLSIZE;
                    tileInstance.y += 32;
                }
            }

            i++;
            isValid = that.isValid();

            // Tried to lay this tile down too many times, just rage-quit
            if(tileInstance.y > 40 * CELLSIZE) {
                break;
            }
        }

        return isValid;
    }

    while(tilePool.length > 0) {
        i = random.boundedInt(0, tilePool.length - 1);
        var tileToLay = tilePool[i];
        tilePool.splice(i, 1);
        this.tiles.push(tileToLay);

        // Try laying it at the "cursor" location (x, y)
        tileToLay.x = x * CELLSIZE;
        tileToLay.y = y * CELLSIZE;

        var placementValid = tryLayingTile(tileToLay);
        if(placementValid === false) {
            // If the placement was not successful
            // dump all the tiles back and quit trying to create
            // the random tile arrangement.
            this.tiles = this.tiles.concat(tilePool);
            return false;
        } else {
            x = tileToLay.x * _CELLSIZE;
            y = tileToLay.y * _CELLSIZE;
        }
    }

    this.game.startGame();
}

/**
 * Tile layout helper functions.
 */
module.exports = function (gameInstance) {
    var tiles = [];
    var size;
    var count;
    var newTileParams;
    for(size in TILELIST) {
        for(count = TILELIST; count > 0; count--) {
            newTileParams = {
                game : gameInstance,
                x : random.boundedInt(0,200),
                y : random.boundedInt(0,200)
            };
            newTileParams[size] = size;
            newTile = new Tile(newTileParams);
            tiles.push(newTile);
        }
    }

    return {
        game       : gameInstance,
        tiles      : tiles,
        finalTiles : [],

        // Tile arrangement x, y and bx, by
        // bx > x and by > y
        x          : -1,
        y          : -1,
        bx         : -1,
        by         : -1,

        updateBounds          : updateBounds,
        isValid               : isValid,
        getTileXYFromMouse    : getTileXYFromMouse,
        startWithRandomLayout : startWithRandomLayout
    };
};