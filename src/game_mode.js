var atom = require('atom');
var CELLSIZE = require('./tile').prototype.CELLSIZE;
var _CELLSIZE = 1 / CELLSIZE;

// Convert mouse position from pixels to tile cells
function translateMouseToLayout(game) {
    return {
        x : Math.floor(Math.floor(atom.input.mouse.x * _CELLSIZE) - game.user.layout.x * CELLSIZE) * _CELLSIZE,
        y : Math.floor(Math.floor(atom.input.mouse.y * _CELLSIZE) - game.user.layout.y * CELLSIZE) * _CELLSIZE
    };
}

function onButtonClick(game) {
    atom.playSound('drop');
    if (game.user.lastButton.clicked) {
        game[game.user.lastButton.clicked]();
    }
}

module.exports = {
    current : 'select',

    gameover : function () {
        if (atom.input.pressed('mouseleft')) {
            if (this.ui.findUIThing('buttons')) {
                onButtonClick(this);
            }
        }
    },

    play : function () {
        if (atom.input.pressed('mouseleft')) {
            var mouse;
            if (this.ui.findUIThing('tiles')) {
                mouse = translateMouseToLayout(this);
                if (
                    this.user.tile === this.user.lastTile ||
                    this.user.tile.getOuterCell(mouse.x, mouse.y) > 0 ||
                    this.user.moves > 0 && mouse.x !== this.user.lastMove.x && mouse.y !== this.user.lastMove.y
                ) {
                    // Already played a move on this tile. Invalid move.
                    this.onBadMove();
                } else {
                    this.onMoveMade();
                }
            } else if (this.ui.findUIThing('buttons')) {
                onButtonClick(this);
            }
        }
    },

    select : function (dt) {
        this.user.lastClickTime += dt;
        if (atom.input.down('mouseleft')) {
            if (this.ui.findUIThing('tiles')) {
                if (this.user.lastTile === this.user.tile && this.user.lastClickTime < 0.3) {
                    // Double click to change orientation of tile under mouse
                    this.user.tile.transposeOrientation();
                    atom.playSound('drop');
                    delete this.user.lastTile;

                } else {
                    // Grabbed tile to move it around for user defined layout
                    this.user.lastClickTime = 0;
                    this.mode.current = 'move';
                    this.user.lastTile = this.user.tile;

                    // Rearrange the tile orders so that the most recently grabbed
                    // tile is rendered last (e.g. highest z-index).
                    var frontIndex = this.tiles.indexOf(this.user.lastTile);
                    if (frontIndex !== this.tiles.length - 1) {
                        var front = this.tile[frontIndex];
                        this.tiles.splice(frontIndex, 1);
                        this.tiles.push(front);
                    }

                    atom.playSound('pick');
                }
            }
        }

        if (atom.input.down('mouseleft') && this.ui.findUIThing('buttons')) {
            onButtonClick(this);
        }
    },

    move : function (dt) {
        this.user.lastClickTime += dt;
        if (atom.input.released('mouseleft')) {
            this.user.lastClickTime = 0;
            this.mode.current = 'select';
            this.user.tile.x = CELLSIZE * Math.round(this.user.tile.x * _CELLSIZE);
            this.user.tile.y = CELLSIZE * Math.round(this.user.tile.y * _CELLSIZE);
            atom.playSound('drop');

            if(this.verifyLayoutValid()) {
                this.enableStartButton();
            } else {
                this.disableStartButton();
            }
        } else {
            // Move the tile with the mouse
            this.user.lastClickTime += dt;
            this.user.tile.x = atom.input.mouse.x - this.user.mouseOffset.x;
            this.user.tile.y = atom.input.mouse.y - this.user.mouseOffset.y;

            var dx = atom.input.mouse.x - this.user.lastMouse.x;
            var dy = atom.input.mouse.y - this.user.lastMouse.y;

            var magnitude = Math.abs(dx) + Math.abs(dy);
            if(magnitude > 0) {
                var sign = dx > 0? 1 : -1;
                var newRotation = Math.log(magnitude) * 0.16 * sign;
                if(-0.09 < newRotation && newRotation < 0.09) {
                    this.user.tile.rotation = newRotation;
                }
            }

            this.user.lastMouse = {
                x : atom.input.mouse.x,
                y : atom.input.mouse.y
            };
        }
    }
};