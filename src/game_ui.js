/**
 * Get UI element under the mouse location.
 * @param {string} elementType
 */
function elementFromMouse(elementType) {
    var queryResult = false;
    var x = atom.input.mouse.x;
    var y = atom.input.mouse.y;

    var user = this.game.user;

    if (elementType === 'tiles') {

        queryResult = this.game.layout.tiles.some(function (tile) {
            if (tile && tile.containsPoint(x, y)) {
                user.tile = tile;
                user.mouseOffset.x = x - tile.x;
                user.mouseOffset.y = y - tile.y;
                return true;
            }
        });
    } else if (elementType === 'buttons') {
        user.lastButton = null;

        var game = this.game;
        queryResult = Object.keys(this.game.buttons).some(function (buttonName) {
            var button = game.buttons[buttonName];
            if (button && button.containsPoint(x, y)) {
                user.lastButton = button;
                return true;
            }
        });
    }

    return queryResult;
}

/** @param gameInstance */
module.exports = function (gameInstance) {
    return {
        game        : gameInstance,
        findUIThing : elementFromMouse
    };
};