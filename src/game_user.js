/**
 * Interaction / User state.
 */
module.exports = {
    // Red   = 1
    // Black = 2
    color : 1,

    lastClickTime : 0,
    lastButton    : null,
    lastMove      : {
        x : -1,
        y : -1
    },

    lastTile : null,
    tile     : null, // currently clicked tile

    layout : {
        x  : 0,
        y  : 0,
        bx : 0, // max X
        by : 0  // max Y
    },

    mouseOffset : {
        x : 0,
        y : 0
    }
};