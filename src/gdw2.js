document.addEventListener('DOMContentLoaded', function () {
    "use strict";

    var atom = require('atom');
    atom.init();

    var Game = require('./game');
    var soundsPreloaded = false;
    var imagesPreloaded = false;

    function onPreloadComplete() {
        if (soundsPreloaded && imagesPreloaded) {
            window.game = new Game();
            window.game.run();
        }
    }

    atom.preloadImages({
        cell_      : 'gfx/cell_.png',
        cell_red   : 'gfx/cell_red.png',
        cell_black : 'gfx/cell_black.png',

        button_random      : 'gfx/button_random.png',
        button_help        : 'gfx/button_help.png',
        button_play        : 'gfx/button_play.png',
        button_fastforward : 'gfx/button_fastforward.png',
        button_rewind      : 'gfx/button_rewind.png',
        button_restart     : 'gfx/button_restart.png'
    }, function () {
        imagesPreloaded = true;
        onPreloadComplete();
    });

    atom.preloadSounds({
        crack      : 'sfx/crack.mp3',
        pick       : 'sfx/pick.mp3',
        drop       : 'sfx/drop.mp3',
        invalid    : 'sfx/invalid.wav',
        valid      : 'sfx/valid.wav',
        gamerules1 : 'sfx/gamerules1.mp3',
        gamerules2 : 'sfx/gamerules2.mp3',
        gamerules3 : 'sfx/gamerules3.mp3',
        gamerules4 : 'sfx/gamerules4.mp3',

        r_damn       : 'sfx/remarks/damn.wav',
        r_damnit     : 'sfx/remarks/damnit.wav',
        r_disturbing : 'sfx/remarks/disturb.wav',
        r_excellent  : 'sfx/remarks/excellent.wav',
        r_fate       : 'sfx/remarks/fate.wav',
        r_good       : 'sfx/remarks/good.wav',
        r_haha       : 'sfx/remarks/haha.wav',
        r_hmm        : 'sfx/remarks/hmm.wav',
        r_longenough : 'sfx/remarks/longenough.wav',
        r_slow       : 'sfx/remarks/slow.wav'
    }, function () {
        soundsPreloaded = true;
        onPreloadComplete();
    });

    window.atom = atom;
});