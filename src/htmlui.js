function HTMLUI(params){
    var key;

    for(key in params) {
        if(params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }

    this.el = document.createElement(this.tagName);

    if(this.style) {
        var styleString = '';
        for(key in this.style) {
            styleString += key + ':' + this.style[key] + ';';
        }

        this.el.setAttribute('style', styleString);
    }

    document.body.appendChild(this.el);
}

HTMLUI.prototype = {
    tagName : 'div'
};

module.exports = HTMLUI;