var atom = require('atom');

function Instructions(params) {
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }

    /**
     * Help message sequence
     * @type Object[]
     */
    this.HELP = [
        this.NEUTRAL_GAMERULES1,
        this.NEUTRAL_GAMERULES2,
        this.NEUTRAL_GAMERULES3,
        this.NEUTRAL_GAMERULES4,
        this.NEUTRAL_GAMERULES5
    ];

    /**
     * Current instruction message
     * @type {string}
     */
    this.current = null;

    /**
     * Display duration of the current instruction message
     * @type {null|number}
     */
    this.timer = null;

    /** @type {number} */
    this.sequenceIndex = 0;

    this.set({
        name : 'NEUTRAL_BOARDSETUP'
    });
}

Instructions.prototype = {
    NEUTRAL_BOARDSETUP : {
        text  : [
            'Construct board layout.',
            '',
            'Drag the tiles to form a board of',
            'non-overlapping tiles. Overlapped',
            'tiles are shown in red. Double click',
            'a tile to change its rotation.'
        ],
        audio : null,
        time  : Infinity
    },
    BAD_BOARDINVALID   : {
        text  : ['Illegal board layout!'],
        audio : null,
        time  : 0
    },
    BAD_MOVEINVALID    : {
        text  : ['Illegal move!'],
        audio : null,
        time  : 100
    },
    NEUTRAL_GAMERULES1 : {
        text  : [
            '[1/5] This game is won by scoring. The',
            'player who owns the most cells wins.',
            'On each turn, a player places a marble',
            'on an empty spot on a tile.'
        ],
        audio : 'gamerules1',
        time  : 700
    },
    NEUTRAL_GAMERULES2 : {
        text  : [
            '[2/5] A player owns a tile when his',
            'marbles take up a majority of the',
            'cells on the tile.'
        ],
        audio : 'gamerules2',
        time  : 400
    },
    NEUTRAL_GAMERULES3 : {
        text  : [
            "[3/5] A player's score is the sum of",
            "the cells in owned tiles."
        ],
        audio : null,
        time  : 400
    },
    NEUTRAL_GAMERULES4 : {
        text  : [
            '[4/5] Players take turns placing one',
            'marble (per turn) in an empty cell on',
            "a tile that doesn't contain the last",
            "move.",
            "A move is legal only if it lies on the",
            "same row or column as the last",
            "marble played."
        ],
        audio : 'gamerules3',
        time  : 900
    },
    NEUTRAL_GAMERULES5 : {
        text  : [
            '[5/5] The game ends when a player has',
            'no legal moves left on their turn.'
        ],
        audio : 'gamerules4',
        time  : 300
    },
    NEUTRAL_REDSTURN   : {
        text  : ["Red's turn."],
        audio : null,
        time  : Infinity
    },
    NEUTRAL_BLACKSTURN : {
        text  : ["Black's turn."],
        audio : null,
        time  : Infinity
    },
    NEUTRAL_GAMEOVER   : {
        text  : ['Game over!'],
        audio : null,
        time  : Infinity
    },

    /** @constant */
    FONT : 'bold 20px Helvetica',

    /** @constant */
    FILLSTYLE : '#222'
};

Instructions.prototype.clear = function () {
    this.current = null;
};

/**
 * Set current instruction item.
 * @param params
 */
Instructions.prototype.set = function (params) {
    this.current = params.name;
    var currentItem = this[this.current];
    if(currentItem instanceof Array) {
        currentItem = currentItem[0];
    }

    this.timer = currentItem.time;
    this.sequenceIndex = 0;
};

Instructions.prototype.prevInSequence = function () {
    this.sequenceIndex--;
    if(this.sequenceIndex < 0) {
        this.sequenceIndex = 0;
        this.game.triggers.disablehelprewind.call(this.game);
    }

    if(this.sequenceIndex === 0) {
        this.game.triggers.disablehelprewind.call(this.game);
    }

    this.timer = this[this.current][this.sequenceIndex].time + 1;
    atom.stopAllSounds();
};

Instructions.prototype.nextInSequence = function () {
    this.sequenceIndex++;
    if(this.sequenceIndex < this[this.current].length) {
        this.timer = this[this.current].time + 1;
        this.game.triggers.enablehelprewind.call(this.game);

    } else {
        atom.stopAllSounds();
        this.clear();
        this.game.triggers.showwhosturn.call(this.game);
    }
};

function playInstructionSound(currentItem) {
    if (currentItem.audio && this.timer === currentItem.time) {
        atom.stopAllSounds();
        atom.playSound(currentItem.audio);
    }
}

Instructions.prototype.draw = function () {
    if (this.current && this.timer > 0) {
        atom.context.font = this.FONT;
        atom.context.fillStyle = this.FILLSTYLE;

        var currentItem = this[this.current];
        playInstructionSound(currentItem);

        for (var i = 0; i < currentItem.text.length; i++) {
            atom.context.fillText(
                currentItem.text[i].toUpperCase(),
                10,
                atom.height - this.vOffset - 20 * (currentItem.text.length - i)
            );
        }

        this.timer--;
    } else if(this.timer === 0) {
        this.nextInSequence();
    }
};

module.exports = Instructions;