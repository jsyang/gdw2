function boundedInt(min, max) {
    return Math.floor(min + Math.random() * (max - min + 1));
}

function itemInArray(a) {
    if (a.length) {
        return a[boundedInt(0, a.length - 1)];
    } else {
        return undefined;
    }
}

function keyInWeightedDict(dict) {
    var sum = 0;
    var _sum;
    var key;
    for (key in dict) {
        sum += dict[key];
    }

    _sum = 1 / sum;
    var rValue = Math.random();

    var intStart = 0;
    var intEnd = 0;
    for (key in dict) {
        if (intEnd === 0) {
            intEnd = dict[key] * _sum;
        } else {
            intEnd += dict[key] * _sum;
        }

        if(intStart <= rValue && rValue <= intEnd) {
            return key;
        } else {
            intStart = intEnd;
        }
    }
}

module.exports = {
    boundedInt        : boundedInt,
    itemInArray       : itemInArray,
    keyInWeightedDict : keyInWeightedDict
};