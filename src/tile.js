var atom = require('atom');

function Tile(params) {
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }

    // Set the width and height based on size string
    var dimensions = this.size.split('x').map(function (v) {
        return parseInt(v, 10);
    }).sort();

    this.w = dimensions[0];
    this.h = dimensions[1];
}

Tile.prototype = {
    // Unit is pixels
    CELLSIZE         : 32,
    x                : 0,
    y                : 0,
    BORDERSIZE       : 4,
    BORDERSIZE_      : 1 / 4,

    // Unit is cells
    w                : 0,
    h                : 0,

    // Coords relative to user.layout
    lx               : 0,
    ly               : 0,
    rotation         : 0,

    // Points this tile is worth
    score            : 0,

    // Is the placement valid?
    invalidPlacement : true,
    finalTallied     : false,
    finalScore       : null,
    finalColor       : null,
    finalTextOffset  : null,
    opacity          : 1,
    firedNextTally   : false,

    INVALIDBORDERCOLOR : '#f00',
    VALIDBORDERCOLOR   : '#333'
};

Tile.prototype.transposeOrientation = function () {
    var oldW = this.w;
    var oldH = this.h;
    this.w = oldH;
    this.h = oldW;
};

Tile.prototype.containsPoint = function (x, y) {
    return this.x <= x && x <= this.x + this.CELLSIZE * this.w &&
        this.y <= y && y <= this.y + this.CELLSIZE * this.h;
};

Tile.prototype.lockOrientation = function () {
    this.score = this.w * this.h;
    this.cells = [];
    for (var i = this.score; i > 0; i--) {
        this.cells.push(0);
    }
};

Tile.prototype.getInnerCell = function (x, y) {
    return this.cells[this.w * y + x];
};

Tile.prototype.getOutterCell = function (x, y) {
    return this.getInnerCell(x - this.lx, y - this.ly);
};

Tile.prototype.setInnerCell = function (x, y, v) {
    this.cells[this.w * y + x] = v;
    return v;
};

Tile.prototype.setOutterCell = function (x, y, v) {
    return this.setInnerCell(x - this.lx, y - this.ly, v);
};

Tile.prototype.getCentroid = function () {
    return {
        x : this.x + this.w * this.CELLSIZE * 0.5,
        y : this.y + this.h * this.CELLSIZE * 0.5
    };
};

Tile.prototype.draw = function () {
    var c = this.getCentroid();
    atom.context.save();

    // Everything is drawn with respect to tile centroid.
    atom.context.translate(c.x, c.y);

    if (this.finalTallied) {
        // Fade the tile out when game over and points are tallied
        if (this.opacity > 0.3) {
            this.opacity -= 0.05;
        }
        atom.context.globalAlpha = this.opacity;
    } else {
        // todo: move this logic out into a separate tile class
        var rotationValue = Math.abs(this.rotation);
        if (rotationValue > 0) {
            this.rotation *= 0.6;
            if (rotationValue < 0.001) {
                this.rotation = 0;
            }
        }

        if (rotationValue !== 0) {
            atom.context.rotate(this.rotation);
        }
    }

    // Draw each cell inside the tile
    var x, y, w, h;
    for (var j = 0; j < this.h; j++) {
        for (var i = 0; i < this.w; i++) {
            x = this.CELLSIZE * (i - this.w * 0.5);
            y = this.CELLSIZE * (j - this.h * 0.5);
            var cellValue = this.cells && this.cells[this.w * j + i];
            var cellImage = 'cell_';
            if (cellValue === 1) {
                cellImage = 'cell_red';
            } else if (cellValue === 2) {
                cellImage = 'cell_black';
            }

            atom.context.drawImage(atom.GFX[cellImage], x, y);
        }
    }

    // Draw borders to indicate invalid placement during layout phase.
    if (this.invalidPlacement) {
        atom.context.strokeStyle = this.INVALIDBORDERCOLOR;
    } else {
        atom.context.strokeStyle = this.VALIDBORDERCOLOR;
    }

    atom.context.lineWidth = this.BORDERSIZE;
    atom.context.lineJoin = 'round';

    x = -0.5 * this.CELLSIZE * this.w + this.BORDERSIZE_ + 1;
    y = -0.5 * this.CELLSIZE * this.h + this.BORDERSIZE_ + 1;
    w = this.w * this.CELLSIZE - this.BORDERSIZE + 2;
    h = this.h * this.CELLSIZE - this.BORDERSIZE + 2;
    atom.context.strokeRect(x, y, w, h);

    // Show points awarded to player who controls this tile.
    if (this.finalTallied && this.opacity <= 0.3) {
        atom.context.globalCompositeOperation = 'source-over';
        atom.context.font = 'bold 30px Helvetica';

        if (
            typeof this.finalScore === 'undefined' ||
            typeof this.finalColor === 'undefined' ||
            typeof this.finalTextOffset === 'undefined'
        ) {
            var redCells = 0;
            var blackCells = 0;
            this.cells.forEach(function (v) {
                if (v === 1) {
                    redCells++;
                } else if (v === 2) {
                    blackCells++;
                }
            });

            this.finalScore = this.score;
            if(redCells === blackCells) {
                this.finalScore = '-';
                this.finalColor = '#666';
            } else if(redCells > blackCells) {
                this.finalColor = '#f00';
            } else {
                this.finalColor = '#222';
            }

            this.finalTextOffset = {
                x : -0.5 * atom.context.measureText(this.finalScore).width,
                y : 10
            };

            atom.context.fillStyle = this.finalColor;
            atom.context.globalAlpha = 1;
            atom.context.fillText(this.finalScore, this.finalTextOffset.x, this.finalTextOffset.y);

            if(!this.firedNextTally) {
                atom.playSound('drop');
                this.firedNextTally = true;
                setTimeout(this.game.triggers.tallyscore.bind(this.game), 300);
            }
        }
    }

    atom.context.restore();
};

module.exports = Tile;