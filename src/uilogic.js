var atom = require('atom');

var HTMLUI = require('./htmlui');
var Tile = require('./tile');
var Button = require('./button');
var random = require('./random');

var COLORS = [null, 'RED', 'BLACK'];
var CELLSIZE = Tile.prototype.CELLSIZE;

module.exports = {
    tallyScore : function () {
        var that = this;
        this.tiles.forEach(function (v) {
            if (!v.finalTallied) {
                that.finaltiles.push(v);
                that.tiles.splice(that.tiles.indexOf(v), 1);
                v.finalTallied = true;
            }
        });
    },

    // React to a player entity making a move.
    onMoveMade : function (coord) {
        this.user.tile.setOutterCell(coord.x, coord.y, this.user.color);
        this.user.lastTile = this.user.tile;

        // Set who should take the next turn
        if (this.user.color === 1) {
            this.user.color = 2;
        } else if (this.user.color === 2) {
            this.user.color = 1;
        }

        this.removeHighlight();
        this.user.lastMove = coord;

        this.user.moves++;
        atom.playSound('crack');

        this.addHighlight();

        // todo: rename this function
        if (this.checkIfPlayerHasMovesLeft()) {
            this.showWhosTurn();

            if (this.ai) {
                this.aistate.updateBoard();
                if (this.user.color === this.ai.color) {
                    this.aistate.calculateTauntBasedOnScore();

                    /**
                     * Make a move only after the turn's done.
                     * Psychologize!
                     * Randomize the computer's move time, since humans will believe
                     * the game is hard if the computer takes a long time to make his move.
                     */
                    setTimeout(this.ai.makeMove.bind(this.ai), random.boundedInt(200, 900));
                }
            }

        } else {
            this.showGameOver();
            alert('No moves left for ' + COLORS[this.user.color]);
            this.calculateScores();
        }
    },

    showGameOver : function () {
        this.instructions.set({name : 'NEUTRAL_GAMEOVER'});
        this.removeHelpButton();
        this.addRestartGameButton();
    },

    onBadMove : function () {
        atom.playSound('invalid');
        this.instructions.set({name : 'BAD_MOVEINVALID'});
        this.removeHelpNavigationButtons();
    },

    showWhosTurn : function () {
        this.instructions.set({name : 'NEUTRAL_' + COLORS[this.user.color] + 'STURN'});
        this.removeHelpNavigationButtons();
    },

    removeHelpNavigationButtons : function () {
        delete this.buttons.fastForward;
        delete this.buttons.rewind;
    },

    disableHelpRewind : function () {
        this.buttons.rewind.color.opacity = 0.5;
        this.buttons.rewind.clicked = null;
    },

    enableHelpRewind : function () {
        delete this.buttons.rewind.color.opacity;
        this.buttons.rewind.clicked = 'onHelpRewind';
    },

    disableHelpFastForward : function () {
        this.buttons.fastforward.color.opacity = 0.5;
        this.buttons.fastforward.clicked = null;
    },

    enableHelpFastForward : function () {
        delete this.buttons.rewind.color.opacity;
        this.buttons.rewind.clicked = 'onHelpFastForward';
    },

    onHelpFastForward : function () {
        this.instructions.nextInSequence();
    },

    onHelpRewind : function () {
        this.instructions.prevInSequence();
    },

    addHelpNavigationButtons : function () {
        this.buttons.fastForward = new Button({
            x       : atom.width - 171,
            y       : atom.height - 60,
            hOffset : 171,
            w       : 61,
            h       : 50,
            image   : 'button_fastforward',
            clicked : 'onHelpFastForward'
        });

        this.buttons.rewind = new Button({
            x       : atom.width - 242,
            y       : atom.height - 60,
            hOffset : 242,
            w       : 61,
            h       : 50,
            image   : 'button_rewind',
            clicked : 'onHelpRewind'
        });

        this.disableHelpRewind();
    },

    addHighlight : function () {
        this.ui.cellHighlight = new HTMLUI({
            style : {
                position : 'absolute',
                left     : ((this.user.layout.x + this.user.lastMove.x) * CELLSIZE) + 'px',
                top      : ((this.user.layout.y + this.user.lastMove.y) * CELLSIZE) + 'px',
                width    : CELLSIZE + 'px',
                height   : CELLSIZE + 'px',
                opacity  : 0.4
            }
        });
    },

    removeHighlight : function () {
        document.body.removeChild(this.ui.cellHighlight);
        delete this.ui.cellHighlight;
    },

    removeRandomLayoutButton : function () {
        delete this.buttons.randomLayout;
    },

    removeHelpButton : function () {
        delete this.buttons.help;
        this.removeHelpNavigationButtons();
    },

    removeStartButton : function () {
        delete this.buttons.start;
    },

    enableStartButton : function () {
        this.buttons.start.clicked = 'startGame';
    },

    disableStartButton : function () {
        this.buttons.start.opacity = 0.2;
        this.buttons.start.clicked = 'onInvalidStart';
    },

    startGame            : function () {
        var that = this;
        this.tiles.forEach(function (tile) {
            tile.lockOrientation();
            tile.lx = tile.x / CELLSIZE - that.user.layout.x;
            tile.ly = tile.y / CELLSIZE - that.user.layout.y;
        });

        this.removeStartButton();
        this.removeRandomLayoutButton();

        this.buttons.help.clicked = 'showGameRulesHelp';

        // Init current game mode and AI player
        this.mode.current = 'play';
        this.user.lastTile = null;
        atom.playSound('valid');

        this.showWhosTurn();
        this.aistate = new AIState({game : this});
        this.ai = new AIPlayer({game : this});

        // todo: move this into AIState as part of the constructor
        this.aistate.setBoard();
    },

    // todo: get rid of this
    generateRandomLayout : function () {
        this.createRandomLayout();
    },

    onInvalidStart : function () {
        atom.playSound('invalid');
    },

    calculateScores : function () {
        // Note: this code is semi-duplicated within AIState to use for taunts
        var scoreRed = 0;
        var scoreBlack = 0;

        var endMessage;
        var that = this;
        this.tiles.forEach(function (tile) {
            var redPoints = 0;
            var blackPoints = 0;
            tile.cells.forEach(function (cell) {
                if (cell === 1) {
                    redPoints++;
                } else if (cell === 2) {
                    blackPoints++;
                }
            });

            if (redPoints > blackPoints) {
                scoreRed += tile.w * tile.h;
            } else if (blackPoints > redPoints) {
                scoreBlack += tile.w * tile.h;
            }
        });

        if (scoreRed > scoreBlack) {
            endMessage = 'Red wins.';
        } else if (scoreRed < scoreBlack) {
            endMessage = 'Black wins.';
        } else {
            endMessage = 'Draw.';
        }

        if (this.ai.color === 2) {
            if (scoreBlack > scoreRed) {
                endMessage += '\nYou lose, human!';
                // One last taunt
                this.aistate.calculateTauntBasedOnScore();
            }
        }

        this.instructions.NEUTRAL_GAMEOVER.text = [
            'GAME OVER!',
            '',
            'Final scores',
            'Red\t' + scoreRed,
            'Black\t' + scoreBlack,
            '',
            endMessage
        ].join('\n');

        this.instructions.set({name : 'NEUTRAL_GAMEOVER'});
        // todo: figure out what the intent of this was..
        this.tallyScore();

        this.mode.current = 'gameover';
    },

    showGameRulesHelp : function () {
        this.instructions.set({name : 'HELP'});
        this.addHelpNavigationButtons();
    },

    showTilePlacementHelp : function () {
        this.instructions.set({name : 'NEUTRAL_BOARDSETUP'});
    },

    addRestartGameButton : function () {
        this.buttons.restart = new Button({
            x       : atom.width - 10 - 136,
            y       : atom.height - 60,
            hOffset : 136,
            w       : 136,
            h       : 50,
            image   : 'button_restart',
            clicked : 'reloadPage'
        });
    },

    reloadPage: window.location.reload.bind(window.location),

    // todo: do this better with HTMLUI

    verticalOrientation : function () {
        this.instructions.vOffset = 50;
        this.reflowButtons();
    },

    horizontalOrientation : function () {
        this.instructions.vOffset = 0;
        this.reflowButtons();
    },

    reflowButtons : function () {
        for(var key in this.buttons) {
            var button = this.buttons[key];
            button.x = atom.width - button.hOffset;
            button.y = atom.height - 60;
        }
    }
};